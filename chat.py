import socket
import time
import threading
import sys
import select
import queue

def getOwnIp(addr):
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.connect((addr, 50005))
	ownIp = sock.getsockname()[0]
	global hostIp
	hostIp = ownIp
	sock.shutdown(socket.SHUT_RDWR)
	sock.close()
	return(ownIp)

def peerConnect(addr):
	try:
		global listOfPeer
		client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		client.connect((addr, 50000))
		client.send(b'!ack')
		listOfPeer.append(client)
		print("{Connected to %s}" %(addr))
	except Exception as e:
		print(e)

def printMessage(data):
	mes = data.decode()
	if(mes == '!ack'):
		return
	print(mes)

# Connects to itself so the threads doesn't block on the socket
# After unblocking the thread it terminates them
def endThreads(thread):
	global threadAlive
	threadAlive = False
	thread[0].join()

	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(b'', ('127.0.0.1', 50005))
	sock.close()
	thread[1].join()

def chatIn():
	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server.bind(('', 50000))
	server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	server.listen(10)
	serverIn = [server]
	serverOut = []
	serverMes = {}
	global threadAlive
	global listOfPeer
	while(threadAlive):
		while(serverIn):
			readable, writeable, exceptional = select.select(serverIn, serverOut, serverMes, 0.1)
			# Inbound
			for s in readable:
				if(s is server):
					clientConn, clientAddr = s.accept()
					listOfPeer.append(clientConn)
					serverIn.append(clientConn)
					serverMes[clientConn] = queue.Queue() 
				else:
					data = s.recv(1024)
					if(data):
						printMessage(data)
						serverMes[s].put(data)
						if(s not in serverOut):
							serverOut.append(s)
					else:
						if(s in serverOut):
							serverOut.remove(s)
						serverIn.remove(s)
						s.close()
						del serverMes[s]
			# Error handler
			for s in exceptional:
				serverIn.remove(s)
				if(s in serverOut):
					serverOut.remove(s)
				s.close()
				del serverMes[s]
			if(threadAlive != True):
				for s in serverIn:
					s.close()
				serverIn = []
				print('Closing TCP Socket')

def chatAlive():
	global threadAlive
	while(threadAlive):
		udpSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		udpSock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		udpSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		udpSock.bind(('', 50005))
		data, addr = udpSock.recvfrom(1024)
		if(data.decode() == '!alive'):
			res = "!res:%s" %(getOwnIp(addr[0]))
			try:
				udpSock.sendto(res.encode(), addr)
			except Exception as e:
				print(e)
		global hostIp
		if(addr[0] != hostIp and addr[0] != '127.0.0.1'):
			peerConnect(addr[0])
		udpSock.close()
	print('Closing UDP Socket')


threadAlive = True
hostIp = ''
listOfPeer = []

chatIn = threading.Thread(target=(chatIn))
chatAlive = threading.Thread(target=(chatAlive))
chatIn.start()
chatAlive.start()

while(True):
	userInput = input('>')
	if(userInput.find('!') == 0):
		# !exit
		if(userInput.find('!exit') == 0):
			endThreads((chatIn, chatAlive))
			print("Goodbye")
			sys.exit()
		# !connect xxx.xxx.xxx.xxx
		if(userInput.find('!conn') == 0):
			idx = userInput.index(' ')+1
			ipAddr = userInput[idx:]
			peerConnect(ipAddr)
		# !close xxx.xxx.xxx.xxx
		elif(userInput.find('!close') == 0):
			idx = userInput.index(' ')+1
			ipAddr = userInput[idx:]
			for sock in listOfPeer:
				if(sock.getpeername()[0] == ipAddr):
					sock.close()
					listOfPeer.remove(sock)
					print("{Disconnected from %s}" %(ipAddr))
		# !alive
		elif(userInput.find('!alive') == 0):
			udpSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			udpSock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
			udpSock.sendto('!alive'.encode(), ('<broadcast>', 50005))
			udpSock.settimeout(3)
			while(True):
				try:
					data, addr = udpSock.recvfrom(1024)
					if not data:
						break
				except socket.timeout:
					break
				if(hostIp == addr[0]):
					continue
				if(data.decode().find('!res') == 0):
					idx = data.decode().index(':')+1
					ipAddr = data.decode()[idx:]
					peerConnect(ipAddr)

			udpSock.close()
			print("{Sended alive command}")
		else:
			print("{Error: Not a Command}")
	else:
		for sock in listOfPeer:
			sock.send(userInput.encode())